package ru.tsc.gavran.tm.constant;

public class TerminalConst {

    public static final String ABOUT = "about";

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String TASK_CREATE = "task_create";

    public static final String TASK_LIST = "task_list";

    public static final String TASK_CLEAR = "task_clear";

    public static final String PROJECT_CREATE = "project_create";

    public static final String PROJECT_LIST = "project_list";

    public static final String PROJECT_CLEAR = "project_clear";

}