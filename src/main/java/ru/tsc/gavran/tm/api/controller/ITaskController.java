package ru.tsc.gavran.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();


}
