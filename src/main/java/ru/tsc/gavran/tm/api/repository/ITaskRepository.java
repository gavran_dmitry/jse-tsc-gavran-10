package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
