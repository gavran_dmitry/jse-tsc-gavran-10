package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
